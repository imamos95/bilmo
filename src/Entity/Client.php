<?php

namespace App\Entity;

use App\Repository\ClientRepository;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Hateoas\Relation ("self",
 *     href = @Hateoas\Route("detailClient", parameters = { "id" = "expr(object.getId())" }),
 *     exclusion = @Hateoas\Exclusion(groups="getClients")
 *     )
 *
 * @Hateoas\Relation ("delete",
 *     href = @Hateoas\Route("deleteClient", parameters = { "id" = "expr(object.getId())" }),
 *     exclusion = @Hateoas\Exclusion(groups="getClients", excludeIf = "expr(not is_granted('ROLE_ADMIN'))"),
 *     )
 *
 * @Hateoas\Relation ("update",
 *     href = @Hateoas\Route("updateClient", parameters = { "id" = "expr(object.getId())" }),
 *     exclusion = @Hateoas\Exclusion(groups="getClients", excludeIf = "expr(not is_granted('ROLE_ADMIN'))"),
 *     )
 */
#[ORM\Entity(repositoryClass: ClientRepository::class)]
class Client
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["getClients"])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["getClients"])]
    #[Assert\NotBlank(message: 'Le nom du client est obligatoire')]
    #[Assert\Length(
        min: 2,
        max: 80,
        minMessage: 'Le nom doit faire au moins {{ limit }} caractères',
        maxMessage: 'Le nom ne peut pas faire plus de {{ limit }} caractères'
    )]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Groups(["getClients"])]
    #[Assert\NotBlank(message: 'L\'email du client est obligatoire')]
    #[Assert\Email(message: 'L\'email {{ value }} n\'est pas un email valide')]
    private ?string $email = null;

    #[ORM\Column(length: 255)]
    #[Groups(["getClients"])]
    #[Assert\NotBlank(message: 'L\'adresse du client est obligatoire')]
    private ?string $address = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["getClients"])]
    #[Assert\NotBlank(message: 'Le numéro de téléphone du client est obligatoire')]
    private ?string $phone = null;

    #[ORM\ManyToOne(inversedBy: 'clients')]
    #[Groups(["getClients"])]
    private ?User $user = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(["getClients"])]
    private ?DateTimeInterface $createdAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(["getClients"])]
    private ?DateTimeInterface $updatedAt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): static
    {
        $this->address = $address;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): static
    {
        $this->phone = $phone;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTimeInterface $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
