<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Attributes as OA;

/**
 * @Hateoas\Relation("self",
 *     href = @Hateoas\Route("detailProduct", parameters = { "id" = "expr(object.getId())" }),
 *     exclusion= @Hateoas\Exclusion(groups="getCustomers"),
 * )
 * @Hateoas\Relation("delete",
 *     href = @Hateoas\Route("deleteProduct",parameters = { "id" = "expr(object.getId())" }),
 *     exclusion= @Hateoas\Exclusion(excludeIf = "expr(not is_granted('ROLE_ADMIN'))"),
 * )
 * @Hateoas\Relation("update",
 *     href = @Hateoas\Route("updateProduct",parameters = { "id" = "expr(object.getId())" }),
 *     exclusion= @Hateoas\Exclusion(excludeIf = "expr(not is_granted('ROLE_ADMIN'))"),
 *)
 */
#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["getProducts"])]
    #[OA\Property(description: 'The unique identifier of the product.')]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["getProducts"])]
    #[Assert\NotBlank(message: 'Le nom du produit est obligatoire')]
    private ?string $name = null;

    #[ORM\Column]
    #[Groups(["getProducts"])]
    #[Assert\NotBlank(message: 'Le prix du produit est obligatoire')]
    private ?float $price = null;

    #[ORM\Column(length: 255)]
    #[Groups(["getProducts"])]
    #[Assert\NotBlank(message: 'La marque du produit est obligatoire')]
    private ?string $brand = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["getProducts"])]
    #[Assert\NotBlank(message: 'La description du produit est obligatoire')]
    #[Assert\Length(min: 20, max: 255,
        minMessage: "La description doit faire au moins {{ limit }} caractères",
        maxMessage: "La description ne peut pas faire plus de {{ limit }} caractères")]
    private ?string $description = null;

    #[ORM\Column(length: 100, nullable: true)]
    #[Groups(["getProducts"])]
    #[Assert\NotBlank(message: 'La couleur du produit est obligatoire')]
    private ?string $color = null;

    #[ORM\Column(length: 255)]
    #[Groups(["getProducts"])]
    #[Assert\NotBlank(message: 'Le modèle du produit est obligatoire')]
    private ?string $model = null;

    #[ORM\Column]
    #[Groups(["getProducts"])]
    #[Assert\NotBlank(message: 'La capacité du produit est obligatoire')]
    private ?int $capacity = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(["getProducts"])]
    private ?DateTimeInterface $createdAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(["getProducts"])]
    private ?DateTimeInterface $updatedAt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): static
    {
        $this->price = $price;

        return $this;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(string $brand): static
    {
        $this->brand = $brand;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): static
    {
        $this->color = $color;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): static
    {
        $this->model = $model;

        return $this;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): static
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?DateTimeInterface $createdAt): static
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTimeInterface $updatedAt): static
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }


}
