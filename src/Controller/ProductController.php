<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Service\VersionService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

class ProductController extends AbstractController
{

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly SerializerInterface    $serializer,
        private readonly VersionService         $versionService,
        private readonly TagAwareCacheInterface $cache,
    )
    {
    }

    #[Route('/api/product', name: 'product', methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: 'Successful response',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: Product::class)))
    )]
    #[OA\Parameter(name: 'page', description: 'The page of the result', in: 'query', required: false, schema: new OA\Schema(type: 'integer', default: 1))]
    #[OA\Parameter(name: 'limit', description: 'Number of elements per page', in: 'query', required: false, schema: new OA\Schema(type: 'integer', default: 3))]
    #[OA\Tag(name: 'Products')]
    public function getAllProducts(ProductRepository $productRepository, Request $request, TagAwareCacheInterface $cache): JsonResponse
    {
        $page = $request->query->get('page', 1);
        $limit = $request->query->get('limit', 10);

        $idCache = "getAllProducts-" . $page . "-" . $limit;
        $products = $cache->get($idCache, function (ItemInterface $item) use ($productRepository, $page, $limit) {
            $item->tag("productsCache");
            return $productRepository->findAllWithPagination($page, $limit);
        });

        $version = $this->versionService->getVersion();

        $context = SerializationContext::create()->setGroups(['getProducts']);
        $context->setVersion($version);
        $jsonProducts = $this->serializer->serialize($products, 'json', $context);

        return new JsonResponse($jsonProducts, Response::HTTP_OK, [], true);
    }

    #[Route('/api/product/{id}', name: 'detailProduct', methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: 'Successful response',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: Product::class)))
    )]
    #[OA\Tag(name: 'Products')]
    public function getDetailProduct(Product $product): JsonResponse
    {
        $version = $this->versionService->getVersion();
        $context = SerializationContext::create()->setGroups(['getProducts']);
        $context->setVersion($version);

        $jsonProduct = $this->serializer->serialize($product, 'json', $context);
        return new JsonResponse($jsonProduct, Response::HTTP_OK, ['accept' => 'json'], true);
    }

    #[Route('/api/product', name: 'createProduct', methods: ['POST'])]
    #[isGranted('ROLE_ADMIN', message: 'Vous n\'avez pas les droits suffisants pour créer un produit')]
    #[OA\Response(
        response: 201,
        description: 'Votre produit a bien été créé',
        content: new OA\JsonContent(
            type: 'array', items: new OA\Items(ref: new Model(type: Product::class)))
    )]
    #[OA\Tag(name: 'Products')]
    public function createProduct(Request $request, UrlGeneratorInterface $urlGenerator, ValidatorInterface $validator): JsonResponse
    {
        $product = $this->serializer->deserialize($request->getContent(), Product::class, 'json');
        $product->setCreatedAt(new DateTime());
        $errors = $validator->validate($product);
        if ($errors->count() > 0) {
            return new JsonResponse($this->serializer->serialize($errors, 'json'),
                Response::HTTP_BAD_REQUEST, [],
                true);
        }
        $this->entityManager->persist($product);
        $this->entityManager->flush();

        $this->cache->invalidateTags(['productsCache']);
        $version = $this->versionService->getVersion();
        $context = SerializationContext::create()->setGroups(['getProducts'])->setVersion($version);
        $jsonProduct = $this->serializer->serialize($product, 'json', $context);

        $location = $urlGenerator->generate(
            'detailProduct',
            ['id' => $product->getId(),
                UrlGeneratorInterface::ABSOLUTE_URL]
        );

        return new JsonResponse($jsonProduct, Response::HTTP_CREATED, ['Location' => $location], true);
    }

    /**
     * @throws InvalidArgumentException
     */
    #[Route('/api/product/{id}', name: 'updateProduct', methods: ['PUT'])]
    #[IsGranted('ROLE_ADMIN', message: 'Vous n\'avez pas les droits suffisants pour mettre à jour ce produit')]
    #[OA\Tag(name: 'Products')]
    #[OA\Response(response: 200, description: 'Votre produit a bien été mis à jour')]
    public function updateProduct(
        Request                $request,
        Product                $product,
        ValidatorInterface     $validator,
        TagAwareCacheInterface $cache,
        UrlGeneratorInterface  $urlGenerator,
    ): JsonResponse
    {
        /** @var Product $newProduct */
        $newProduct = $this->serializer->deserialize($request->getContent(),
            Product::class,
            'json');

        $product->setName($newProduct->getName())
            ->setPrice($newProduct->getPrice())
            ->setBrand($newProduct->getBrand())
            ->setDescription($newProduct->getDescription())
            ->setColor($newProduct->getColor())
            ->setModel($newProduct->getModel())
            ->setCapacity($newProduct->getCapacity())
            ->setCreatedAt($newProduct->getCreatedAt())
            ->setUpdatedAt(new DateTime());

        $errorsUpdate = $validator->validate($newProduct);
        if ($errorsUpdate->count() > 0) {
            return new JsonResponse($this->serializer->serialize($errorsUpdate, 'json'),
                Response::HTTP_BAD_REQUEST, [], true);
        }

        $this->entityManager->persist($product);
        $this->entityManager->flush();

        $cache->invalidateTags(['productsCache']);

        $version = $this->versionService->getVersion();
        $context = SerializationContext::create()->setGroups(['getProducts'])->setVersion($version);

        $jsonProduct = $this->serializer->serialize($product, 'json', $context);
        $location = $urlGenerator->generate(
            'detailClient',
            ['id' => $product->getId()],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        return new JsonResponse($jsonProduct, Response::HTTP_OK, ['Location' => $location], true);
    }


    #[Route('/api/product/{id}', name: 'deleteProduct', methods: ['DELETE'])]
    #[IsGranted('ROLE_ADMIN', message: 'Vous n\'avez pas les droits suffisants pour supprimer ce produit')]
    #[OA\Response(response: 204, description: 'Votre produit a bien été supprimé')]
    #[OA\Response(response: 404, description: 'Produit non trouvé')]
    #[OA\Tag(name: 'Products')]
    public function deleteProduct(Product $product = null): JsonResponse
    {
        if (!$product) {
            $data = [
                'error' => 'Produit non trouvé',
                'message' => 'Le produit avec l\'ID spécifié n\'existe pas.',
            ];
            return new JsonResponse($this->serializer->serialize($data, 'json'), Response::HTTP_NOT_FOUND, [], true);
        }
        $this->entityManager->remove($product);
        $this->entityManager->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
