<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\User;
use App\Repository\ClientRepository;
use App\Service\VersionService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;


class ClientController extends AbstractController
{

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly SerializerInterface    $serializer,
        private readonly TagAwareCacheInterface $cache,
        private readonly VersionService         $versionService
    )
    {
    }

    /**
     * @throws InvalidArgumentException
     */
    #[Route('/api/clients', name: 'client', methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: 'Liste des clients',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(
                ref: new Model(type: Client::class, groups: ['getClients'])))
    )]
    #[OA\Tag(name: 'Clients')]
    public function getClients(Request $request, ClientRepository $clientRepository,): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        $page = $request->get('page', 1);
        $limit = $request->get('limit', 3);

        $idCache = 'getClients-' . $user->getId();

        $clientsList = $this->cache->get($idCache, function (ItemInterface $item) use ($clientRepository, $page, $limit, $user) {
            $item->tag('clientsCache');
            return $clientRepository->findAllWithPagination($page, $limit, $user);
        });

        $version = $this->versionService->getVersion();
        $context = SerializationContext::create()->setGroups(['getClients']);
        $context->setVersion($version);

        $jsonClientsList = $this->serializer->serialize($clientsList, 'json', $context);

        return new JsonResponse($jsonClientsList, Response::HTTP_OK, [], true);
    }

    #[Route('/api/clients/{id}', name: 'detailClient', methods: ['GET'])]
    #[OA\Tag(name: 'Clients')]
    #[OA\Response(
        response: 200,
        description: 'Successful response',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(
                ref: new Model(type: Client::class, groups: ['getClients'])))
    )]
    public function getDetailClient(Client $client): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($client->getUser()->getId() !== $user->getId()) {
            return new JsonResponse('Vous n\'avez pas les droits suffisants pour accéder à ce client', Response::HTTP_FORBIDDEN);
        }
        $version = $this->versionService->getVersion();
        $context = SerializationContext::create()->setGroups(['getClients']);
        $context->setVersion($version);

        $jsonClient = $this->serializer->serialize($client, 'json', $context);

        return new JsonResponse($jsonClient, Response::HTTP_OK, [], true);
    }


    /**
     * @throws InvalidArgumentException
     */
    #[Route('/api/clients', name: 'createClient', methods: ['POST'])]
    #[IsGranted('ROLE_ADMIN', message: 'Vous n\'avez pas les droits suffisants pour créer un client')]
    #[OA\Tag(name: 'Clients')]
    #[OA\Response(response: 201,
        description: 'Votre client a bien été créé',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: Client::class, groups: ['getClients'])))
    )]
    public function createClient(Request $request, UrlGeneratorInterface $urlGenerator, ValidatorInterface $validator): JsonResponse
    {
        $client = $this->serializer->deserialize($request->getContent(), Client::class, 'json');
        $client->setUser($this->getUser());
        $client->setCreatedAt(new DateTime());
        $errors = $validator->validate($client);

        if ($errors->count() > 0) {
            return new JsonResponse(
                $this->serializer->serialize($errors, 'json'),
                Response::HTTP_BAD_REQUEST, [],
                true
            );
        }
        $this->entityManager->persist($client);
        $this->entityManager->flush();

        $this->cache->invalidateTags(['clientsCache']);

        $version = $this->versionService->getVersion();

        $context = SerializationContext::create()->setGroups(['getClients'])->setVersion($version);

        $jsonClient = $this->serializer->serialize($client, 'json', $context);

        $location = $urlGenerator->generate(
            'detailClient',
            ['id' => $client->getId()],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        return new JsonResponse($jsonClient, Response::HTTP_CREATED, ['Location' => $location], true);
    }

    /**
     * @throws InvalidArgumentException
     */
    #[Route('/api/clients/{id}', name: 'updateClient', methods: ['PUT'])]
    #[IsGranted('ROLE_ADMIN', message: 'Vous n\'avez pas les droits suffisants pour mettre à jour ce client')]
    #[OA\Tag(name: 'Clients')]
    #[OA\Response(response: 200, description: 'Votre client a bien été mis à jour')]
    public function updateClient(
        Request $request,
        Client $currentClient,
        UrlGeneratorInterface $urlGenerator,
        ValidatorInterface $validator
    ): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($currentClient->getUser()->getId() !== $user->getId()) {
            return new JsonResponse('vous n\'avez pas un client avec cette ID', Response::HTTP_FORBIDDEN);
        }

        /** @var Client $newClient */
        $newClient = $this->serializer->deserialize($request->getContent(),
            Client::class,
            'json');

        $currentClient->setName($newClient->getName())
            ->setEmail($newClient->getEmail())
            ->setPhone($newClient->getPhone())
            ->setAddress($newClient->getAddress())
            ->setCreatedAt($newClient->getCreatedAt())
            ->setUpdatedAt(new DateTime());

        $errorsUpdate = $validator->validate($newClient);

        if ($errorsUpdate->count() > 0) {
            return new JsonResponse($this->serializer->serialize($errorsUpdate, 'json'), Response::HTTP_BAD_REQUEST, [], true);
        }

        $this->entityManager->persist($currentClient);
        $this->entityManager->flush();
        $this->cache->invalidateTags(['clientsCache']);

        $version = $this->versionService->getVersion();

        $context = SerializationContext::create()->setGroups(['getClients'])->setVersion($version);

        $jsonClient = $this->serializer->serialize($currentClient, 'json', $context);
        $location = $urlGenerator->generate(
            'detailClient',
            ['id' => $currentClient->getId()],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        return new JsonResponse($jsonClient, Response::HTTP_OK, ['Location' => $location], true);
    }

    /**
     * @throws InvalidArgumentException
     */
    #[Route('/api/clients/{id}', name: 'deleteClient', methods: ['DELETE'])]
    #[IsGranted('ROLE_ADMIN', message: 'Vous n\'avez pas les droits suffisants pour supprimer ce client')]
    #[OA\Response(response: 204, description: 'Votre client a bien été supprimé')]
    #[OA\Tag(name: 'Clients')]
    public function deleteClient(Client $client): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($client->getUser()->getId() !== $user->getId()) {
            return new JsonResponse('vous n\'avez pas un client avec cette ID', Response::HTTP_FORBIDDEN);
        }

        if (!$client) {
            $data = [
                'error' => 'Produit non trouvé',
                'message' => 'Le client avec l\'ID spécifié n\'existe pas.',
            ];
            return new JsonResponse($this->serializer->serialize($data, 'json'), Response::HTTP_NOT_FOUND, [], true);
        }
        $this->entityManager->remove($client);
        $this->entityManager->flush();
        $this->cache->invalidateTags(['clientsCache']);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
