<?php

namespace App\DataFixtures;

use App\Entity\Client;
use App\Entity\Product;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{

    public function __construct(private readonly UserPasswordHasherInterface $userPasswordHasher)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        // Création d'un user "normal"

        $user = new User();
        $user->setEmail("user@bilemo.com")
            ->setRoles(["ROLE_USER"])
            ->setFirstname($faker->firstName())
            ->setLastname($faker->lastName())
            ->setPassword($this->userPasswordHasher->hashPassword($user, "password"));
        $manager->persist($user);

        // Création d'un user admin
        $userAdmin = new User();
        $userAdmin->setEmail("admin@bilemo.com")
            ->setRoles(["ROLE_ADMIN"])
            ->setFirstname($faker->firstName())
            ->setLastname($faker->lastName())
            ->setPassword($this->userPasswordHasher->hashPassword($userAdmin, "password"));
        $manager->persist($userAdmin);

        for ($j = 1; $j < 20; $j++) {
            $client = new Client();
            $client->setName($faker->company())
                ->setEmail($faker->companyEmail())
                ->setAddress($faker->address())
                ->setPhone($faker->phoneNumber())
                ->setUser($faker->randomElement([$user, $userAdmin]))
                ->setCreatedAt($faker->dateTimeBetween('-6 months', '-1 week'));
            $manager->persist($client);
        }


        for ($i = 1; $i < 30; $i++) {
            $product = new Product();
            $product->setName($faker->name())
                ->setPrice($faker->randomFloat(2, 0, 1000))
                ->setDescription($faker->text())
                ->setBrand($faker->company())
                ->setModel($faker->name())
                ->setCapacity($faker->randomNumber(3))
                ->setColor($faker->colorName())
            ->setCreatedAt($faker->dateTimeBetween('-6 months', '-1 week'));
            $manager->persist($product);
        }

        $manager->flush();
    }
}
